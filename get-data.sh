#!/bin/bash
wget --no-verbose -r --no-parent http://felsinfo.alpenverein.de/kletterfelsen/

pushd felsinfo.alpenverein.de

# delete index files
find -name "index.html*" -delete
# remove files with broken encoding in filename (ruby optparse does not like this)
LC_ALL=C find . -name '*[! -~]*' -delete

popd