#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'erb'
require 'optparse'

options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: transform.rb --template <template> files..."

    opts.on("-tTEMPLATE", "--template=TEMPLATE", "Template") do |v|
        options[:template] = v
    end
end.parse!

def read_page(url)
    page = Nokogiri::HTML(open(url))
    /(?<name>Fels.*)/ =~ page.css('title').text
    info_text = page.css('#allgemein > div .kategorie_titel,.felsinfo').text
    /Geogr.\ Länge\s*(?<longitude>\d+\.\d+)\s*°/m =~ info_text
    /Geogr.\ Breite\s*(?<latitude>\d+\.\d+)\s*°/m =~ info_text
    {
        name: name,
        url: 'http://%s' % url,
        latitude: latitude,
        longitude: longitude,
    }
end

rocks = ARGV.map! { |x| read_page(x) }

def template_binding(rocks)
    include ERB::Util
    binding
end

puts ERB.new(File.read(options[:template])).result(template_binding(rocks))