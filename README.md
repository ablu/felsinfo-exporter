Aggregates data from http://felsinfo.alpenverein.de/kletterfelsen/ and exports a GPX track which can be imported into apps like https://osmand.net/

# Usage:

Download the website data:

```
./get-data.sh
```

Generate GPX with all rocks:
```
find -name "*.html" -type f -printf '%P\n'|xargs -s $(getconf ARG_MAX) ./transform.rb --template template.gpx > "Felsinfo full.gpx"
```